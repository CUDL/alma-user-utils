'''

Usage: userxml2json convert [options] [(-s|-m)] [<userxml>]
       userxml2json stream [options] [(-s|-m)] [<userxml>...]

Description:

Convert Alma user XML into their JSON representation. If the convert subcommand
is used, the output is a JSON representation of the input document, whether a
user or users object. If the stream subcommand is used, each user in the list
of specified user/users files will be output as a single JSON user object in
a newline-delimited JSON stream.

Options:
    <userxml>
        A file containing Alma User XML or Users XML (rooted with <user> or
        <users> element).

    -s,--single
    -m,--multiple
        Expect <userxml> to be one <user> element, or a <users> element
        containing multiple <user> elements.

    --traceback
        Print exception/traceback info on fatal errors.


'''

from os.path import join, dirname
import functools
import sys
import json
import traceback

from docopt import docopt
from lxml import etree

from .userxml2json import userxml2json
from .usersxml2json import usersxml2json
from almauserutils import jsonstreams


@functools.lru_cache(maxsize=1)
def user_parser():
    schema_path = join(dirname(__file__), 'data', 'rest_user.xsd')
    return etree.XMLParser(schema=etree.XMLSchema(file=schema_path))


@functools.lru_cache(maxsize=1)
def users_parser():
    schema_path = join(dirname(__file__), 'data', 'rest_users.xsd')
    return etree.XMLParser(schema=etree.XMLSchema(file=schema_path))


class Args:
    _default_single = True

    _props = {'single', 'multiple', 'userxml'}

    def __init__(self, docopt_args):
        self.da = docopt_args

    @property
    def input_file(self):
        assert len(self.userxml) in [0, 1]
        return self.userxml[0] if len(self.userxml) else sys.stdin

    @property
    def input_file_name(self):
        if hasattr(self.input_file, 'name'):
            return self.input_file.name
        return self.input_file

    @property
    def json_indent(self):
        return 4

    @property
    def is_single(self):
        assert not (self.single and self.multiple)

        if not self.single and not self.multiple:
            return self._default_single

        return self.single

    def __getattr__(self, x):
        for key in ['--{}'.format(x), '<{}>'.format(x), x]:
            if key in self.da:
                return self.da[key]
        raise AttributeError('No arg: {}'.format(x))


def parse_user_xml(file):
    return etree.parse(file, parser=user_parser())


def parse_users_xml(file):
    return etree.parse(file, parser=users_parser())


def write_json(obj, args):
    json.dump(obj, sys.stdout, indent=args.json_indent)

    if args.json_indent is not None:
        print()


def fatal(msg, args, exc=None, file=sys.stderr):
    if args.traceback and exc is not None:
        traceback.print_exception(None, exc, exc.__traceback__, None, file)
        print(file=file)

    print('Fatal: {}'.format(msg), file=file)


def main():
    args = Args(docopt(__doc__))

    try:
        if args.stream:
            if args.is_single:
                single_users = (parse_user_xml(input_file)
                                for input_file in args.userxml)

                users = (userxml2json(userxml2json(xml_user))
                         for xml_user in single_users)
            else:
                users_objects = (parse_users_xml(input_file)
                                 for input_file in args.userxml)

                users = (user
                         for users_xml in users_objects
                         for user in usersxml2json(users_xml)['user'])

            jsonstreams.dump(users, sys.stdout.buffer)
        else:
            if args.is_single:
                user = parse_user_xml(args.input_file)
                write_json(userxml2json(user), args)
            else:
                users = parse_users_xml(args.input_file)
                write_json(usersxml2json(users), args)
    except etree.XMLSyntaxError as e:
        name = 'user' if args.is_single else 'users'
        fatal('Unable to parse input XML: {} does not contain a valid {} '
              'element'.format(args.input_file_name, name), args, exc=e)
        exit(2)
