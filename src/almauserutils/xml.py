from lxml import etree


def get_users(fileobj):
    doc = etree.parse(fileobj)

    return doc.xpath('/users/user')


def create_users_xml(users):
    root = etree.Element('users')

    for user in users:
        root.append(user)

    return root


def get_identifiers(user):
    '''
    Get the set of identifiers associated with a user. Identifiers are pairs of
    (id type, value) where id type is either 'primary' or the lowercase version
    of the alternative ID type.
    '''
    additional_ids = get_additional_identifiers(user)
    return {('primary', get_primary_id(user))} | set(additional_ids)


def get_additional_identifiers(user):
    ids = user.xpath('''\
./user_identifiers/user_identifier[
    normalize-space(value[1]) and normalize-space(id_type[1])]''')

    return [(i.find('id_type').text.lower(), i.find('value').text)
            for i in ids]


def get_primary_id(user):
    primary_id = user.find('primary_id')
    return primary_id is not None and primary_id.text
