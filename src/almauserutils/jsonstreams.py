from functools import wraps
from io import TextIOWrapper
import json

__all__ = ['load', 'dump', 'dump_incremental']


def load(fp):
    '''
    Lazily deserialise a newline-delimited stream of JSON objects.

    Arguments:
        fp: A file-like object with a .read() method
    Returns:
        A generator which lazily loads and returns one JSON object at a time.
    '''

    with TextIOWrapper(UnclosableFile(fp), encoding='utf-8') as input:
        for line in input:
            yield json.loads(line)


def dump(objects, fp, flush=True):
    '''
    Lazily serialise the sequence of JSON-serialisable objects as a
    newline-delimited stream of JSON objects.

    Arguments:
        objects: A sequence of objects to be serialised (may be a generator)
        fp: A file-like object to write the JSON stream to
        flush: If True, flush immediately after each object is written
    '''

    dumper = dump_incremental(fp, flush=flush)
    for object in objects:
        dumper.send(object)


def prewarmed(generator):
    @wraps(generator)
    def prewarmer(*args, **kwargs):
        gen = generator(*args, **kwargs)
        gen.send(None)
        return gen
    return prewarmer


@prewarmed
def dump_incremental(fp, flush=True):
    with TextIOWrapper(UnclosableFile(fp), encoding='utf-8',
                       line_buffering=bool(flush)) as output:
        while True:
            object = yield
            if object is not None:
                json.dump(object, output, indent=None, separators=(',', ':'))
                output.write('\n')



# We need this because io's wrappers close their wrapped file objects when
# GCd or close()d - we don't want that, the callers are responsible for closing.
class UnclosableFile:
    '''
    Wrap a file-like object to make its close() method do nothing.
    '''
    def __init__(self, target):
        self.target = target

    def close(self):
        pass

    def __getattr__(self, attr):
        return getattr(self.target, attr)
