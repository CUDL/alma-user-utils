from copy import deepcopy

from .xmlconversion import (
    Converter, MatcherRule, AttributeMatcherRule, ElementAndParentTagMatcher,
    AttributeAndParentTagMatcher, Attr)
from .userxml2json import userxml2json


class DelegateUserConversionRule(MatcherRule):
    def apply(self, match):
        el = match.obj

        extracted_el = deepcopy(el)
        return userxml2json(extracted_el)


class UsersRule(MatcherRule):
    def apply(self, match):
        el = match.obj
        result = {
            'user': [match.converter.convert(e) for e in el]
        }

        result.update(match.converter.convert(Attr(el, attr))
                      for attr in el.attrib)

        return result


users_converter = Converter([
    DelegateUserConversionRule(ElementAndParentTagMatcher([
        (('users', 'user'), None)
    ])),
    UsersRule(ElementAndParentTagMatcher([
        ((None, 'users'), None)
    ])),
    AttributeMatcherRule(AttributeAndParentTagMatcher([
        ((None, 'users', 'total_record_count'), 'int')
    ]))
])


def usersxml2json(users):
    return users_converter.convert(
        users.getroot() if hasattr(users, 'getroot') else users)
