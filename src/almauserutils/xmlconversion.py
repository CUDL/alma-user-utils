from abc import ABC, abstractmethod
from collections import namedtuple
from functools import wraps
from itertools import chain

from lxml import etree


class Element(ABC):
    pass
Element.register(type(etree.Element('foo')))


class Attr(namedtuple('Attr', 'element name')):
    '''
    Represents an attribute on an Element.
    '''
    __slots__ = ()


class Attribute(ABC):
    pass
Attribute.register(Attr)


class Converter(object):
    def __init__(self, rules):
        self.rules = list(rules)

    def match(self, obj, after=None):
        '''
        Get the next rule matching obj. If after is provided, only rules not
        already matched after eligible for matching.

        Args:
            obj: The object to match
            after: A MatchedRule which was previously produced by this
                   converter
        '''
        rules = self.rules
        if after is not None:
            exclusions = after.previously_matched_rules
            rules = (r for r in self.rules if r not in exclusions)

        for rule in rules:
            matches, data = rule.try_match(obj)
            if matches is True:
                return MatchedRule(self, after, rule, obj, data)

        raise NoMatch('No rule matches: {}'.format(obj))

    def convert(self, obj):
        return self.match(obj).apply()


class NoMatch(Exception):
    pass


class Rule(ABC):
    def match(self, obj):
        matches, data = self.try_match(obj)

        if matches is not True:
            raise NoMatch()

        return data

    @abstractmethod
    def try_match(self, obj):
        '''
        Determine if obj can be handled by this Rule.

        Args:
            obj: The object to match against
        Returns:
            A two-tuple of (matches, data) where matches is a bool indicating
            if obj matched, and data is arbitrary data related to the match.
        '''
        pass

    @abstractmethod
    def apply(self, match):
        pass


class MatchedRule(ABC):
    '''
    A rule matched against an object.

    Attributes:
        converter: The Converter that invoked the match
        previous_match: The match that preceded this match when using the after
                        argument to Converter.match().
        rule: The rule which matched the object
        obj: The object which the rule matched
        data: Arbitrary data associated with the match
    '''
    __sentinel = object()

    def __init__(self, converter, previous_match, rule, obj, data):
        self.converter = converter
        self.previous_match = previous_match
        self.rule = rule
        self.obj = obj
        self.data = data

    def apply(self):
        return self.rule.apply(self)

    def next_match(self, obj=__sentinel):
        '''
        Get the next matching rule.

        Args:
            obj: Use this object when matching rather than the previously
                 matched object.
        Returns: A new MatchedRule containing the new match.
        Raises:
            NoMatch: If no remaining rule matches the obj
        '''
        return self.converter.match(
            self.obj if obj is MatchedRule.__sentinel else obj, after=self)

    @property
    def previously_matched_rules(self):
        matched = set()
        if self.previous_match is not None:
            matched = self.previous_match.previously_matched_rules
        matched.add(self.rule)
        return matched


class MatcherRule(Rule):
    def __init__(self, matcher):
        if not isinstance(matcher, Matcher):
            raise ValueError('matcher is not an Matcher: {!r}'.format(matcher))
        self.matcher = matcher

    def try_match(self, obj):
        return self.matcher.match(obj)


class LeafRule(MatcherRule):
    def apply(self, match):
        el, data_type = match.obj, match.data

        assert len(el.attrib) == 0

        return (etree.QName(el).localname, convert_el_value(el, data_type))


class LeafWithAttributesRule(MatcherRule):
    def apply(self, match):
        el, data_type = match.obj, match.data

        result = {
            'value': convert_el_value(el, data_type)
        }

        if 'value' in el.attrib:
            raise ValueError('Element has attribute named "value" which '
                             'clashes with "value" key for element\'s body: {}'
                             .format(element_info(el)))

        result.update(match.converter.convert(Attr(el, attr_name))
                      for attr_name in el.attrib)
        return (etree.QName(el).localname, result)


class InternalRule(MatcherRule):
    def apply(self, match):
        el = match.obj

        attr_entries = [
            match.converter.convert(Attr(el, attr))
            for attr in el.attrib
        ]

        entries = [
            match.converter.convert(e)
            for e in el
        ]

        try:
            return (etree.QName(el).localname,
                    json_object(chain(attr_entries, entries)))
        except ValueError as e:
            raise ValueError('Error converting element: {}'
                             .format(element_info(el))) from e


class ListRule(MatcherRule):

    def _strip_name(self, convert_result):
        name, value = convert_result
        return value

    def apply(self, match):
        el, singular_name = match.obj, match.data

        assert len(el.attrib) == 0

        return (singular_name,
                [self._strip_name(match.converter.convert(e)) for e in el])


class AttributeMatcherRule(MatcherRule):
    def apply(self, match):
        (host_el, attr_name), dtype = match.obj, match.data

        return (attr_name, convert_value(host_el.attrib[attr_name], dtype))


class Matcher(ABC):
    def matches(self, obj):
        return self.match(obj)[0]

    @abstractmethod
    def match(self, obj):
        pass


class KeyMatcher(Matcher):
    def __init__(self, key, entries):
        self.key = key
        self.entries = dict(entries)

    def match(self, el):
        key_val = self.key(el)
        try:
            return (True, self.entries[key_val])
        except KeyError:
            return (False, None)


def require_type(type):
    '''
    Decorate a function f(x) such that f(x) returns None if x is not of the
    specified type. As a result, the decorated function is only called with
    instances of the specified type.
    '''
    def _create_decorator(f):
        @wraps(f)
        def require_type_wrapper(val):
            if isinstance(val, type):
                return f(val)
            return None
        return require_type_wrapper
    return _create_decorator


class ElementAndParentTagMatcher(KeyMatcher):
    def __init__(self, entries):
        super().__init__(self._key, entries)

    @staticmethod
    @require_type(Element)
    def _key(el):
        parent = el.getparent()
        return (None if parent is None else parent.tag), el.tag


class AttributeAndParentTagMatcher(KeyMatcher):
    def __init__(self, entries):
        super().__init__(self._key, entries)

    @staticmethod
    @require_type(Attribute)
    def _key(attr):
        host_el, attr_name = attr
        parent_el = host_el.getparent()
        parent_tag = None if parent_el is None else parent_el.tag

        return (parent_tag, host_el.tag, attr_name)


def convert_el_value(el, type):
    return convert_value(extract_el_text(el), type)


def convert_value(val, type):
    try:
        return {
            'str': str,
            'int': int,
            'float': float,
            'bool': parse_bool
        }[type](val)
    except KeyError:
        raise ValueError('Unknown type: {}'.format(type))


def extract_el_text(el, default=''):
    # Elements containing comments also trigger this. Strictly speaking
    # we should handle and ignore comments, but our data never contains
    # comments,
    if len(el) > 0:
        raise ValueError('Leaf element contained unexpected children: {}'
                         .format(element_info(el)))
    return default if el.text is None else el.text


def parse_bool(val):
    try:
        return {'true': True, 'false': False}[val.lower()]
    except KeyError:
        raise ValueError('Cannot interpret val as a bool: {}'
                         .format(val))


def json_object(entries):
    result = {}

    for (k, v) in entries:
        if k in result:
            raise ValueError('Duplicate key: {}'.format(k))
        result[k] = v

    return result


def element_info(el):
    if el.sourceline is not None:
        return '{} on line {}'.format(el.tag, el.sourceline)
    return el.tag


class ObjectKey(namedtuple('ObjectKey', 'value')):
    '''
    Represents a named key in a JSON object.
    '''
    __slots__ = ()


class ArrayIndex(namedtuple('ArrayIndex', 'value')):
    '''
    Represents an integer index in a JSON array.
    '''
    __slots__ = ()


def any_array_element(path_element):
    return isinstance(path_element, ArrayIndex)


class JsonValue(namedtuple('JsonValue', 'path value')):
    '''
    Represents a value at a specific position in a JSON data structure.

    Attributes:
        path: A sequence of ObjectKey and ArrayIndex objects representing the
              location of the value in the structure
        value: The JSON value at the path
    '''
    __slots__ = ()


class AnyJsonMatcher(Matcher):
    def match(self, obj):
        return (isinstance(obj, JsonValue), None)


class AncestorsJsonMatcher(Matcher):
    _unindexable = '__unindexable__'

    def __init__(self, entries):
        (self._full_index, self._partial_index, self._key_layouts,
         self._key_sizes) = self._build_index(entries)

    def _indexable_representation(self, pattern_element):
        if pattern_element is None or isinstance(pattern_element,
                                                 (ObjectKey, ArrayIndex)):
            return pattern_element

        if not callable(pattern_element):
            raise ValueError(
                'pattern element was not indexable or callable: {!r}'
                .format(pattern_element))

        return self._unindexable

    def _build_index(self, entries):
        full_index = {}
        partial_index = {}
        key_layouts = set()
        for (pattern, data) in entries.items():
            key = tuple(self._indexable_representation(pe) for pe in pattern)
            key_repr = tuple(k == self._unindexable for k in key)
            key_layouts.add(key_repr)

            if all(kr is False for kr in key_repr):
                if key in full_index:
                    raise ValueError('duplicate entry: {!r}'.format(key))
                full_index[key] = data
            else:
                entries = partial_index.get(key, [])
                entries.append((pattern, data))
                partial_index[key] = entries

        return (full_index, partial_index, key_layouts,
                sorted(len(kl) for kl in key_layouts))

    def match(self, obj):
        for key_val in self._keys(obj):
            if key_val is None:
                return

            if key_val in self._full_index:
                return (True, self._full_index[key_val])

            for l in self._key_layouts:
                if len(key_val) != len(l):
                    continue

                partial_key = tuple(self._unindexable if is_unindexable else ke
                                    for is_unindexable, ke in _zip(l, key_val))
                candidates = self._partial_index.get(partial_key, [])

                for (pattern, data) in candidates:
                    if all(pe(ke) for is_unindexable, pe, ke
                           in _zip(l, pattern, key_val)
                           if is_unindexable):
                        return (True, data)
        return (False, None)

    def _keys(self, obj):
        return (self._key(obj, n) for n in self._key_sizes)

    @staticmethod
    def _key(json_value, ancestor_count):
        if not isinstance(json_value, JsonValue):
            raise ValueError('json_value is not a JsonValue: {}'
                             .format(json_value))
        if ancestor_count <= 0:
            raise ValueError('ancestor_count must be positive: {}'
                             .format(ancestor_count))

        path = tuple(json_value.path[-ancestor_count:])
        return ((None,) * (ancestor_count - len(path))) + path


def _zip(*args):
    assert all(len(x) == len(args[0]) for x in args[1:])
    return zip(*args)


class IdentityJsonRule(MatcherRule):
    '''
    A rule which returns the matched JSON object without changes, while
    applying the converter rules to all children.
    '''
    def __init__(self, matcher=AnyJsonMatcher()):
        super().__init__(matcher)

    def apply(self, match):
        path, value = match.obj

        if isinstance(value, dict):
            return dict(
                (k, match.converter.convert(JsonValue(path + [ObjectKey(k)],
                                                      v)))
                for k, v in value.items()
            )
        elif isinstance(value, list):
            return [
                match.converter.convert(JsonValue(path + [ArrayIndex(i)], v))
                for i, v in enumerate(value)
            ]
        else:
            return value


class ValueSubstitutionJsonRule(MatcherRule):
    def __init__(self, matcher, substitution_groups):
        super().__init__(matcher)
        self.substitution_groups = substitution_groups

    def apply(self, match):
        (path, value), group = match.obj, match.data

        try:
            key = 'default' if group is None else group
            substitutions = self.substitution_groups[key]
        except KeyError:
            raise ValueError('no such substitution group: {}'.format(group))

        if value in substitutions:
            value = substitutions[value]

        return match.next_match(JsonValue(path, value)).apply()


class DefaultPropertiesJsonRule(MatcherRule):
    '''
    A rule which creates object properties with default values if they're not
    already present on a matched object.

    The matcher's match data must be a dict containing the default keys and
    values to be applied to an object.
    '''

    def apply(self, match):
        (path, value), defaults = match.obj, match.data

        if not isinstance(value, dict):
            raise ValueError('JSON value at path: {!r} is not a dict: {!r}'
                             .format(path, value))

        defaulted_value = {}
        defaulted_value.update(defaults)
        defaulted_value.update(value)

        return match.next_match(JsonValue(path, defaulted_value)).apply()
