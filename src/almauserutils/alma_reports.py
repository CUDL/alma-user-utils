from xml.etree import ElementTree

import requests
from requests import codes
from requests.adapters import HTTPAdapter
from requests.compat import urlencode, quote
from urllib3 import Retry


class AlmaReports:
    ns = {
        'xsd': 'http://www.w3.org/2001/XMLSchema',
        'rowset': 'urn:schemas-microsoft-com:xml-analysis:rowset'
    }

    transport = HTTPAdapter(
        pool_connections=2,
        pool_maxsize=10,
        max_retries=Retry(
            total=5,
            connect=2,
            read=2,
            status=2,
            redirect=2,
            backoff_factor=0.5,
            method_whitelist=Retry.DEFAULT_METHOD_WHITELIST,
            status_forcelist={
                codes.internal_server_error, codes.service_unavailable,
                codes.bad_gateway, codes.gateway_timeout,
                codes.request_timeout, codes.too_many_requests
            }
        )
    )

    def __init__(self, api_endpoint, api_key):
        self._api_endpoint = api_endpoint
        self._session = requests.Session()
        self._session.verify = True
        self._session.mount(api_endpoint, self.transport)
        self._session.headers.update({
            'Accept': 'application/xml',
            'Authorization': 'apikey ' + api_key,
        })

    def open(self, report_path):
        params = urlencode({'path': report_path, 'limit': 1000}, quote_via=quote)
        path = self._api_endpoint + '/analytics/reports'

        response = self._session.get(path, params=params, timeout=(3, None))
        response.raise_for_status()

        root = ElementTree.fromstring(response.content)
        columns = {
            '{urn:schemas-microsoft-com:xml-analysis:rowset}' + e.get('name'):
                e.get('{urn:saw-sql}columnHeading')
            for e in root.iterfind('.//xsd:element', namespaces=self.ns)}

        token = root.findtext('.//ResumptionToken')
        is_finished = root.findtext('.//IsFinished') == 'true'
        for row in root.iterfind('.//rowset:Row', namespaces=self.ns):
            yield {columns[child.tag]: child.text for child in row}

        while not is_finished:
            response = self._session.get(path,
                                         params={'token': token, 'limit': 1000},
                                         timeout=(3, 10))
            response.raise_for_status()
            root = ElementTree.fromstring(response.content)
            is_finished = root.findtext('.//IsFinished') == 'true'
            for row in root.iterfind('.//rowset:Row', namespaces=self.ns):
                yield {columns[child.tag]: child.text for child in row}
