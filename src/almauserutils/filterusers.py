'''

Usage: filterusers [options] [-v|-vv] (-i=<idval> | --ids-from=<idsfile>)...
                   <userxml>...

Options:
    -i=<idval>
        A value in the form <id_type>:<id_value>, where id_type is either
        "primary" or a lowercase code value from the User Identifier Type
        code table. e.g. "primary:hwtb2@cam.ac.uk"

    --ids-from=<idsfile>
        A file containing whitespace-separated ID values as described in
        the -i option.

    <userxml>
        An Alma users xml file. The root element is <users>, which contains
        0 or more <user> elements, conforming to the Alma user V2 format,
        as described at:

            https://developers.exlibrisgroup.com/alma/apis/xsd/rest_user.xsd?tags=GET

    -o=<destfile>, --output=<destfile>
        Write filtered user XML to this location. - is stdout [Default: -]

    --no-indent
        Don't indent the output XML

    -v
        Be more verbose (repeat twice to be even more verbose)
'''

import re
import sys
import io

import docopt

from . import xml


id_pattern = re.compile('^(\w+):(.*)$')


class InvalidIdError(ValueError):
    pass


def parse_id(val):
    match = id_pattern.match(val)
    if not match:
        raise InvalidIdError(
            'ID val did not conform to <id_type>:<id_value> format: {}'
            .format(val))
    return match.groups()


def parse_id_file(file):
    with io.open(file) if isinstance(file, str) else file as fileobj:
        for line_no, line in enumerate(fileobj, 1):
            for val in line.split():
                try:
                    yield parse_id(val)
                except InvalidIdError as e:
                    msg = 'Invalid ID in {} on line {}: {}'.format(
                        fileobj.name, line_no, val)
                    raise InvalidIdError(msg) from e


def resolve_stdin(files):
    '''
    Resolve - to stdin, but only once.
    '''
    return [sys.stdin if f == '-' else f for f in files]


def get_ids(args):
    cmdline_ids = set(parse_id(val) for val in args['-i'])
    file_ids = set(i for file in resolve_stdin(args['--ids-from'])
                   for i in parse_id_file(file))

    return cmdline_ids | file_ids


def all_users(files):
    return (user for file in resolve_stdin(files)
            for user in xml.get_users(file))


def count_iter(iter):
    count = None

    def count_iter():
        i = -1
        for i, val in enumerate(iter):
            yield val

        nonlocal count
        count = i + 1

    def count_reporter():
        if count is None:
            raise RuntimeError('counted iterable is yet to complete')

        return count

    return count_iter(), count_reporter


def users_with_ids(users, ids):
    return (u for u in users if xml.get_identifiers(u) & ids)


def write_output(args, xml_root):
    dest = args['--output']
    if dest == '-':
        dest = io.open(sys.stdout.fileno(), mode='wb')

    is_pretty = not args['--no-indent']

    xml_root.write(dest, encoding='utf-8', pretty_print=is_pretty)


def main():
    args = docopt.docopt(__doc__)

    try:
        ids = get_ids(args)

        if args['-v'] > 0:
            maybe_colon = ':' if args['-v'] > 1 else ''
            print('Found {} IDs{}'.format(len(ids), maybe_colon),
                  file=sys.stderr)

            if args['-v'] > 1:
                print('\n'.join('    {}:{}'.format(n, v)
                                for n, v in sorted(get_ids(args))), '\n',
                      file=sys.stderr)

        users, users_count = count_iter(
            all_users(resolve_stdin(args['<userxml>'])))

        matched_users, matched_count = count_iter(
            users_with_ids(users, ids))

        write_output(args, xml.create_users_xml(matched_users).getroottree())

        if args['-v'] > 0:
            print('Matched {} of {} users'
                  .format(matched_count(), users_count()), file=sys.stderr)
    except (InvalidIdError, OSError) as e:
        print('Error: {}'.format(e), file=sys.stderr)
        sys.exit(1)
