import logging
import socket
import ssl
import sys

import time
from datetime import datetime, timedelta
from decimal import Decimal


class PatronInformation:
    """
    Parse a SIP2 Patron Information response message.
    http://multimedia.3m.com/mws/media/355361O/sip2-protocol.pdf
    https://developers.exlibrisgroup.com/alma/integrations/selfcheck/sip2
    """
    _field_defs = {
        b'AO': ('institution_id', str),
        b'AA': ('patron_identifier', str),
        b'AE': ('personal_name', str),
        b'CA': ('overdue_limit', int),
        b'BL': ('valid_patron', bool),
        b'BH': ('currency', str),
        b'BV': ('fee_amount', Decimal),
        b'CC': ('fee_limit', Decimal),
        b'BD': ('home_address', str),
        b'BE': ('email_address', str),
        b'BF': ('home_phone', str),
        b'AF': ('message', str)
    }
    _blank = b' '[0]

    @staticmethod
    def try_int(value):
        try:
            return int(value)
        except ValueError:
            return None

    def __init__(self, data: bytes):
        if data[0:2] != b'64':
            raise RuntimeError(data.decode().strip())

        status = data[2:16]
        self.charge_denied = status[0] != self._blank
        self.hold_denied = status[3] != self._blank
        self.max_charged = status[5] != self._blank
        self.max_overdue = status[6] != self._blank
        self.max_fines = status[10] != self._blank
        self.max_fees = status[11] != self._blank

        counts = data[37:61]
        self.hold_count = self.try_int(counts[0:4])
        self.overdue_count = self.try_int(counts[4:8])
        self.charged_count = self.try_int(counts[8:12])
        self.fines_count = self.try_int(counts[12:16])

        fields = data[61:]
        for field in fields.split(b'|'):
            key = field[0:2]
            value = field[2:].decode()
            try:
                field_def = self._field_defs[key]
            except LookupError:
                continue
            if field_def[1] == bool:
                value = value == 'Y'
            else:
                value = field_def[1](value)
            setattr(self, field_def[0], value)
        for field_def in self._field_defs.values():
            if not hasattr(self, field_def[0]):
                setattr(self, field_def[0], None)


class SIP2Client:
    """
    Maintain a SIP2 (Self Check) connection to Alma.
    Currently only supports Patron Information requests.

    Not thread-safe.
    """
    language = '001'
    log = logging.getLogger(__name__)
    socket_keepalive = timedelta(minutes=5)
    socket_timeout = timedelta(seconds=10)
    _connected = False

    default_ca = '''
-----BEGIN CERTIFICATE-----
MIIEXjCCAsagAwIBAgIEUJt91jANBgkqhkiG9w0BAQUFADBwMQswCQYDVQQGEwJJ
TDESMBAGA1UECBMJSmVydXNhbGVtMRIwEAYDVQQHEwlKZXJ1c2FsZW0xETAPBgNV
BAoTCEV4bGlicmlzMQ0wCwYDVQQLEwRBbG1hMRcwFQYDVQQDEw5UQ1AgU1NMIFNl
cnZlcjAgFw0xMjExMDgwOTM5MzRaGA8yMTEyMTAxNTA5MzkzNFowcDELMAkGA1UE
BhMCSUwxEjAQBgNVBAgTCUplcnVzYWxlbTESMBAGA1UEBxMJSmVydXNhbGVtMREw
DwYDVQQKEwhFeGxpYnJpczENMAsGA1UECxMEQWxtYTEXMBUGA1UEAxMOVENQIFNT
TCBTZXJ2ZXIwggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQCdKcoDvl23
eFBD6R/yorRQheC4rFWkjV9O4oiF+eZQw3dy1cpCz4RP/vXbglgshZiZ+z6/JqmZ
KEW8P5Go/G3rCf2zXztTtYJi6BIJ0z87NljMQ5q7QVqak9xrAC5Eg7IFqMTrxLv/
HDM3L4+uwdtjcshtDT1VTysAYAVLGYv7pi6PmfkdU9icqQCFcYgKMeVCztvbLu0s
xMXpHiAfKMsIo9RFUcHxo8N8jlopOIblZbo3hjYIzBJkdOqfB2Ctqd6jc7ISEEdG
REI8MjZrNgK2KitAyi1+/cLwAzvPuf2byFB9Taiu8X6NQBvGo+6Uxh7BCruA4H0U
Bxk6Zhq6ziOVOBXo6HLiOdas25/pz/ThVqR4K1kFOcruD7nKi9Qwf84fksbeyQfT
ZUBeqJ6p38B050QPVV+/trdfQJ87SzAn6L3ysD+a7Sg5yhsJv5flj/mBwu1zW+i5
vC4Gga3CNKP+yrEQSxpsW/Eak21T7zowCBIkYCj2VtyDuGaZh/ogIxMCAwEAATAN
BgkqhkiG9w0BAQUFAAOCAYEAHXeE0Vr5cbcjXEnU8zOnCACvhnIchwtZfAD5KZV9
3l59JZPiM+6b7BBVwg+kvcV+9JwHGhPw130+z8LMlJMl6jAazYc2nDM52UoI6pkM
KHyTr9bFGRk4YOTyEL7DcXABITN3kspQ3V6BxrrE+qTdiG5rZerQVhtZ8uOwbh0j
07zixvVNQ4+MqGWU1dTVDiaCIqZDZR2RjDnlNyXUdYRGh7aVl+IaR2Zfvs+TAnS1
AzQgtz3NDhZgjFwFroaSIPSz5D46zk+fpP0aMxFL5GuvkAP3cxVyU1Gcm01OSbBK
vwaK7T/QFWXNYyJtlQPo7N7VeTVT4EmDmxi5/2eKzFiDwCUW2r11feaH728CGmUQ
/A3z1kUIH0cSfGbrQ/QfkJvYrjUVS+9Tv/8QTTZrev6PyhNr2wJhOi/8QFkLkLW7
PNUt8qrtINjOQ9UVmaP38cfm8wW1Rm+AqVhSHVe/P5rc7CJ7pzS1bD3kR6asxJ7o
M33SwqLh14zcuvOGjHN98NtM
-----END CERTIFICATE-----
'''

    def __init__(self, host, port, client_cert, server_cert=None):
        self.host = host
        self.port = port

        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        context.verify_mode = ssl.CERT_REQUIRED
        context.check_hostname = False  # The only name is "TCP SSL Server"
        context.options |= ssl.OP_NO_COMPRESSION
        context.set_ciphers('HIGH:!aNULL:!SSLv2:!RC4:!3DES:!MD5')
        context.load_cert_chain(client_cert)
        if server_cert:
            context.load_verify_locations(cafile=server_cert)
        else:
            context.load_verify_locations(cadata=self.default_ca)

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_keepalive(sock, self.socket_keepalive)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        sock.settimeout(int(self.socket_timeout.total_seconds()))
        self._socket = context.wrap_socket(sock)

    def __del__(self):
        self.close()

    @staticmethod
    def set_keepalive(sock, interval):
        if isinstance(interval, timedelta):
            interval = interval.total_seconds()
        interval = int(interval)

        sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 3)
        if sys.platform == 'linux':
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, interval)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, interval)
        elif sys.platform == 'win32':
            sock.ioctl(socket.SIO_KEEPALIVE_VALS, (1, interval, interval))
        elif sys.platform == 'darwin' or 'bsd' in sys.platform:
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPALIVE, interval)

    def connect(self):
        self._socket.connect((self.host, self.port))
        self._connected = True

    def close(self):
        self._connected = False
        self._socket.close()

    def patron_information(self, identifier, retries=1):
        assert self._connected
        date = datetime.utcnow().strftime('%Y%m%d0000%H%M%S')
        summary = ' ' * 10
        try:
            self._socket.send(
                f'63{self.language}{date}{summary}AA{identifier}|'.encode())
            data = self._socket.recv(2 * 1024)
            self.log.debug('%s', data)
        except socket.error:
            self._connected = False
            if retries > 0:
                time.sleep(1)
                self.connect()
                return self.patron_information(identifier, retries - 1)
            else:
                raise
        else:
            return PatronInformation(data)


def main():
    import argparse
    import simplejson as json

    parser = argparse.ArgumentParser(
        description='Get patron information via SIP2')
    parser.add_argument('identifier', help='Patron identifier')
    parser.add_argument('--cert', required=True,
                        help='Path to client certificate')
    parser.add_argument('--host', default='alma.exlibrisgroup.com')
    parser.add_argument('--port', type=int, default=6443)
    parser.add_argument('--ca', help="Path to Alma's server CA - defaults to"
                                     " their self-signed 'TCP SSL Server'")
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--pretty', action='store_true')
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    client = SIP2Client(args.host, args.port, args.cert, args.ca)
    client.connect()
    info = client.patron_information(args.identifier)

    json.dump(info.__dict__, fp=sys.stdout,
              indent=args.pretty, sort_keys=args.pretty)


if __name__ == '__main__':
    main()
