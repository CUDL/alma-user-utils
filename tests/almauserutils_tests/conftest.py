import json
from os import path
from unittest.mock import MagicMock, sentinel


from lxml import etree
import pytest

from almauserutils.xmlconversion import Matcher


@pytest.fixture()
def match_result():
    return sentinel.match_result


@pytest.fixture(scope='function')
def pos_matcher(match_result):
    matcher = MagicMock(spec=Matcher)
    matcher.match.return_value = (True, match_result)
    return matcher


@pytest.fixture()
def users_json():
    with open(path.join(path.dirname(__file__),
                        './data/sample-users-obj.fmt.json')) as f:
        return json.load(f)


@pytest.fixture()
def users_xml_file():
    return path.join(path.dirname(__file__), './data/sample-users-obj.fmt.xml')


@pytest.fixture()
def users_xml(users_xml_file):
    return etree.parse(users_xml_file)


@pytest.fixture()
def user_json():
    with open(path.join(path.dirname(__file__),
                        './data/sample-user-obj-hwtb2.fmt.json')) as f:
        return json.load(f)


@pytest.fixture()
def user_xml_file():
    return path.join(path.dirname(__file__),
                     './data/sample-user-obj-hwtb2.fmt.xml')


@pytest.fixture()
def user_xml(user_xml_file):
    return etree.parse(user_xml_file)
