import subprocess
import io
import json
import itertools

import pytest

from almauserutils import jsonstreams


def test_cmd_on_path():
    assert subprocess.check_call('which userxml2json'.split()) == 0


def parse_json(bytes):
    return json.loads(bytes.decode('utf-8'))


@pytest.mark.parametrize('type', [['--multiple'], ['-m']])
def test_converts_multiple_users_with_input_file(users_xml_file, users_json,
                                                 type):
    result = subprocess.check_output(['userxml2json', 'convert'] + type +
                                     [users_xml_file])

    assert parse_json(result) == users_json


@pytest.mark.parametrize('type', [['--multiple'], ['-m']])
def test_converts_multiple_users_with_input_on_stdin(users_xml_file,
                                                     users_json, type):

    with open(users_xml_file, 'rb') as f:
        input = f.read()
        result = subprocess.check_output(['userxml2json', 'convert'] + type,
                                         input=input)

    assert parse_json(result) == users_json


@pytest.mark.parametrize('type', [['--single'], ['-s'], []])
def test_converts_single_user_with_input_file(user_xml_file, user_json, type):
    result = subprocess.check_output(['userxml2json', 'convert'] + type +
                                     [user_xml_file])

    assert parse_json(result) == user_json


@pytest.mark.parametrize('type', [['--single'], ['-s'], []])
def test_converts_single_user_with_input_on_stdin(user_xml_file, user_json,
                                                  type):

    with open(user_xml_file, 'rb') as f:
        input = f.read()
        result = subprocess.check_output(['userxml2json', 'convert'] + type,
                                         input=input)

    assert parse_json(result) == user_json


@pytest.mark.parametrize('types', [
    list(p) for p in itertools.product(['convert', 'stream'],
                                       ['--single', '-s'],
                                       ['--multiple', '-m'])])
def test_single_and_multiple_cant_be_combined(types):
    with pytest.raises(subprocess.CalledProcessError) as excinfo:
        subprocess.check_output(['userxml2json'] + types,
                                stderr=subprocess.STDOUT)


@pytest.mark.parametrize('type,count', list(itertools.product(
    [['--multiple'], ['-m']],
    [1, 3]
)))
def test_streams_multiple_users_with_input_file(users_xml_file, users_json,
                                                 type, count):
    result = subprocess.check_output(['userxml2json', 'stream'] + type +
                                     [users_xml_file] * count)

    users = list(jsonstreams.load(io.BytesIO(result)))
    assert users == [
        user for uj in [users_json] * count for user in uj['user']]
