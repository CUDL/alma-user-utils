from io import BytesIO
from unittest.mock import MagicMock

import pytest

from almauserutils.jsonstreams import load, dump, dump_incremental, prewarmed


@pytest.fixture()
def json_stream_bytes():
    return b'''\
{}\n\
{"abc":"def"}\n\
[]\n'''


@pytest.fixture()
def json_stream_objects():
    return [
        {},
        {'abc': 'def'},
        []
    ]


def test_dump(json_stream_objects, json_stream_bytes):
    out = BytesIO()
    dump((o for o in json_stream_objects), out)
    assert not out.closed

    assert out.getvalue() == json_stream_bytes


def test_load(json_stream_objects, json_stream_bytes):
    input = BytesIO(json_stream_bytes)
    assert list(load(input)) == json_stream_objects
    assert not input.closed


def test_prewarmed():
    func = MagicMock()
    warmed_func = prewarmed(func)

    gen = warmed_func()

    func.assert_called_once_with()
    gen.send.assert_called_once_with(None)


def test_dump_incremental(json_stream_objects, json_stream_bytes):
    out = BytesIO()
    dumper = dump_incremental(out)

    for o in json_stream_objects:
        dumper.send(o)

    assert not out.closed

    assert out.getvalue() == json_stream_bytes
