from unittest.mock import MagicMock, sentinel, patch, call

import pytest
from lxml import etree

from almauserutils.xmlconversion import (
    Converter,
    Matcher, KeyMatcher, ElementAndParentTagMatcher,
    AttributeAndParentTagMatcher,
    Rule, MatcherRule, LeafRule, LeafWithAttributesRule,
    InternalRule, ListRule, AttributeMatcherRule, MatchedRule, NoMatch,
    extract_el_text, convert_el_value, convert_value, parse_bool,
    require_type, Attr, Attribute, Element,
    ObjectKey, ArrayIndex, JsonValue, AnyJsonMatcher,
    AncestorsJsonMatcher, IdentityJsonRule, ValueSubstitutionJsonRule,
    DefaultPropertiesJsonRule, any_array_element
)


def create_match(converter=None, previous=None, rule=None, obj=None,
                 data=None):
    return MatchedRule(converter, previous, rule, obj, data)


def test_Rule_match_raises_when_try_match_doesnt_match():
    class TestRule(Rule):
        def try_match(self, obj):
            return (False, None)

        def apply(self, match):
            assert False

    with pytest.raises(NoMatch):
        TestRule().match(sentinel.obj)


def test_MatcherRule_delegates_to_matcher(pos_matcher):
    class TestRule(MatcherRule):
        def apply(self, match):
            assert False

    assert TestRule(pos_matcher).match(sentinel.obj)
    pos_matcher.match.assert_called_once_with(sentinel.obj)


@pytest.mark.parametrize('content, dtype, expected', [
    ('abc', 'str', 'abc'),
    ('123', 'int', 123),
    ('123.5', 'float', 123.5),
    ('true', 'bool', True),
    ('false', 'bool', False)
])
def test_LeafRule(content, dtype, expected, pos_matcher):
    el = etree.fromstring('<a><b/></a>').xpath('/a/b')[0]
    el.text = content
    lr = LeafRule(pos_matcher)
    assert lr.apply(create_match(obj=el, data=dtype)) == ('b', expected)


def test_LeafWithAttributesRule_invokes_converter_on_attributes(pos_matcher):
    converter = MagicMock()
    converter.convert.return_value = (sentinel.key, sentinel.value)
    rule = LeafWithAttributesRule(pos_matcher)

    el = etree.Element('abc', attrib=dict(a='x', b='y', c='z'))
    el.text = 'val'

    rule.apply(create_match(converter=converter, obj=el, data='str'))

    converter.convert.assert_has_calls([
        call(Attr(el, 'a')),
        call(Attr(el, 'b')),
        call(Attr(el, 'c'))
    ])


@pytest.mark.parametrize('content, attrs, expected', [
    ('abc', {}, {'value': 'abc'}),
    ('abc', {'a': 'aval'}, {'value': 'abc', 'a': 'aval'}),
    ('abc', {'a': 'aval', 'b': 'bval'},
        {'value': 'abc', 'a': 'aval', 'b': 'bval'})
])
def test_LeafWithAttributesRule(content, attrs, expected, pos_matcher):
    converter = MagicMock()
    converter.convert.side_effect = lambda attr: (
        attr[1], attr[0].attrib[attr[1]])

    rule = LeafWithAttributesRule(pos_matcher)

    el = etree.Element('x', attrib=attrs)
    el.text = content

    assert (
        rule.apply(create_match(converter=converter, obj=el, data='str')) ==
        ('x', expected))


@pytest.mark.parametrize('xml, expected', [
    (
        '''
        <foo>
            <a>A</a>
            <b>B</b>
            <c>C</c>
        </foo>
        ''',
        ('foo', {
            'a': 'A',
            'b': 'B',
            'c': 'C'
        })
    ),
    (
        '''
        <foo x="X" y="Y">
            <a>A</a>
            <b>B</b>
            <c>C</c>
        </foo>
        ''',
        ('foo', {
            'a': 'A',
            'b': 'B',
            'c': 'C',
            'x': 'X',
            'y': 'Y'
        })
    )
])
def test_InternalRule(pos_matcher, xml, expected):
    def convert(val):
        if isinstance(val, Attribute):
            return (val.name, val.element.attrib[val.name])
        assert isinstance(val, Element)
        return (val.tag, val.text)

    converter = MagicMock()
    converter.convert.side_effect = convert

    el = etree.fromstring(xml)

    rule = InternalRule(pos_matcher)
    assert rule.apply(create_match(converter=converter, obj=el)) == expected


def test_ListRule(pos_matcher):
    converter = MagicMock()
    converter.convert.side_effect = lambda el: (
        'a', el.attrib['x'])
    rule = ListRule(pos_matcher)

    el = etree.fromstring('''
        <foo>
            <a x="1"/>
            <a x="2"/>
            <a x="3"/>
        </foo>
    ''')
    name = 'foo'

    assert (rule.apply(create_match(converter=converter, obj=el, data=name)) ==
            (name, ['1', '2', '3']))


def test_Matcher_matches_delegates_to_match():

    class TestMatcher(Matcher):
        def match(self, x):
            pass

    with patch.object(TestMatcher, 'match') as m:
        m.return_value = (True, None)
        matcher = TestMatcher()

        assert matcher.matches(sentinel.obj)
        matcher.match.assert_called_once_with(sentinel.obj)


@pytest.mark.parametrize('match_data', [
    sentinel.match_data,
    # Should still match with None as data
    None
])
def test_KeyMatcher_matches_key_values_in_entries(match_data):
    key = MagicMock()
    key.return_value = sentinel.key_value

    matcher = KeyMatcher(key, {sentinel.key_value: match_data})

    assert matcher.match(sentinel.obj) == (True, match_data)
    key.assert_called_once_with(sentinel.obj)


def test_KeyMatcher_doesnt_match_key_values_not_in_entries():
    key = MagicMock()
    key.return_value = sentinel.key_value

    matcher = KeyMatcher(key, {sentinel.other_value: 123})

    assert matcher.match(sentinel.obj) == (False, None)
    key.assert_called_once_with(sentinel.obj)


def test_AttributeMatcherRule(pos_matcher):
    rule = AttributeMatcherRule(pos_matcher)

    el = etree.Element('foo', attrib={'bar': 'baz'})
    with patch('almauserutils.xmlconversion.convert_value') as cv:
        cv.return_value = sentinel.convert_result

        assert (rule.apply(create_match(obj=(el, 'bar'),
                                        data=sentinel.data_type)) ==
                ('bar', sentinel.convert_result))

        cv.assert_called_once_with('baz', sentinel.data_type)


@pytest.mark.parametrize('val, type, result', [
    ('abc', str, sentinel.result),
    (123, int, sentinel.result),
    (123, str, None),
    ('abc', int, None)
])
def test_require_type_decorator(val, type, result):
    @require_type(type)
    def func(x):
        return sentinel.result

    assert func(val) == result


@pytest.mark.parametrize('el', [
    etree.fromstring('<a><b/></a>').xpath('//b')[0],
    etree.fromstring('<a:a xmlns:a="x"><a:b/></a:a>')
         .xpath('//a:b', namespaces={'a': 'x'})[0]
])
def test_ElementAndParentTagMatcher_matches_named_elements(el):
    assert ElementAndParentTagMatcher([
        (('a', 'b'), None),
        (('{x}a', '{x}b'), None)
    ]).matches(el)


@pytest.mark.parametrize('el, attr', [
    (etree.fromstring('<a><b k="v"/></a>').xpath('//b')[0], 'k'),
    (
        etree.fromstring('<a:a xmlns:a="x"><a:b a:k="v"/></a:a>')
             .xpath('//a:b', namespaces={'a': 'x'})[0],
        '{x}k'
    ),
    (etree.fromstring('<a k="v"/>'), 'k')
])
def test_AttributeAndParentTagMatcher(el, attr):
    assert AttributeAndParentTagMatcher([
        (('a', 'b', 'k'), None),
        (('{x}a', '{x}b', '{x}k'), None),
        ((None, 'a', 'k'), None)
    ]).matches(Attr(el, attr))


def test_extract_el_text():
    assert extract_el_text(etree.fromstring('<foo>bar</foo>')) == 'bar'


@pytest.mark.parametrize('el', [
    etree.fromstring('<foo>abc<bar/></foo>'),
    etree.fromstring('<foo>abc<bar/>def</foo>'),
    etree.fromstring('<foo>abc<!-- hi --></foo>'),
    etree.fromstring('<foo>abc<!-- hi -->def</foo>')
])
def test_extract_el_text_rejects_elements_with_children(el):
    with pytest.raises(ValueError):
        extract_el_text(el)


@pytest.mark.parametrize('content, dtype, expected', [
    ('abc', 'str', 'abc'),
    ('123', 'int', 123),
    ('123.5', 'float', 123.5),
    ('true', 'bool', True),
    ('false', 'bool', False)
])
def test_convert_el_value(content, dtype, expected):
    el = etree.Element('foo')
    el.text = content

    assert convert_el_value(el, dtype) == expected


@pytest.mark.parametrize('content, dtype, expected', [
    ('abc', 'str', 'abc'),
    ('123', 'int', 123),
    ('123.5', 'float', 123.5),
    ('true', 'bool', True),
    ('false', 'bool', False)
])
def test_convert_value(content, dtype, expected):
    assert convert_value(content, dtype) == expected


@pytest.mark.parametrize('value, expected', [
    ('true', True),
    ('True', True),
    ('TRUE', True),
    ('false', False),
    ('False', False),
    ('FALSE', False)
])
def test_parse_bool(value, expected):
    assert parse_bool(value) == expected


@pytest.mark.parametrize('value', [
    'true_',
    ' true',
    'false_',
    ' false',
    '',
    'SDF'
])
def test_extract_el_bool_fails_with_ambiguous_values(value):
    with pytest.raises(ValueError):
        parse_bool(value)


def test_Converter_convert_raises_on_unmatched_input():
    with pytest.raises(NoMatch):
        Converter([]).convert(123)


def test_Converter_convert_applies_match_result():
    with patch.object(Converter, 'match') as match:
        match().apply.return_value = sentinel.apply_result

        assert Converter([]).convert(sentinel.obj) == sentinel.apply_result

        match.assert_called_with(sentinel.obj)


def test_Converter_match_evaluates_rules_in_order():
    def match_r1(x):
        r2.try_match.assert_not_called()
        r3.try_match.assert_not_called()
        return False, None

    def match_r2(x):
        r1.try_match.assert_called_with(sentinel.obj)
        r3.try_match.assert_not_called()
        return False, None

    def match_r3(x):
        r1.try_match.assert_called_with(sentinel.obj)
        r2.try_match.assert_called_with(sentinel.obj)
        return False, None

    r1 = MagicMock(spec=Rule)
    r2 = MagicMock(spec=Rule)
    r3 = MagicMock(spec=Rule)
    r1.try_match.side_effect = match_r1
    r2.try_match.side_effect = match_r2
    r3.try_match.side_effect = match_r3

    with pytest.raises(NoMatch):
        Converter([r1, r2, r3]).match(sentinel.obj)

    r1.try_match.assert_called_with(sentinel.obj)
    r2.try_match.assert_called_with(sentinel.obj)
    r3.try_match.assert_called_with(sentinel.obj)


def test_Converter_match_result():
    rule = MagicMock(spec=Rule)
    rule.try_match.return_value = True, sentinel.match_data

    conv = Converter([rule])
    match = conv.match(sentinel.obj)

    assert match.converter is conv
    assert match.rule is rule
    assert match.obj is sentinel.obj
    assert match.data is sentinel.match_data
    assert match.previous_match is None


def test_Converter_match_ignores_previously_matched_rule():
    r1 = MagicMock(spec=Rule)
    r1.try_match.return_value = True, sentinel.match_1
    r2 = MagicMock(spec=Rule)
    r2.try_match.return_value = True, sentinel.match_2

    conv = Converter([r1, r2])
    match_1 = conv.match(sentinel.obj_1)
    assert match_1.rule is r1
    assert match_1.obj is sentinel.obj_1
    assert match_1.data is sentinel.match_1

    match_2 = conv.match(sentinel.obj_2, after=match_1)
    assert match_2.rule is r2
    assert match_2.obj is sentinel.obj_2
    assert match_2.data is sentinel.match_2

    with pytest.raises(NoMatch):
        # no more rules available
        conv.match(sentinel.obj_3, after=match_2)


def test_Converter_match_returns_first_matching_rule():
    r1 = MagicMock(spec=Rule)
    r2 = MagicMock(spec=Rule)
    r3 = MagicMock(spec=Rule)

    r1.try_match.return_value = False, None
    r2.try_match.return_value = True, None

    assert Converter([r1, r2, r3]).match(sentinel.obj).rule is r2

    r3.try_match.assert_not_called()


@pytest.mark.parametrize('prev_rule_count', [0, 1, 2, 5])
def test_MatchResult_previously_matched_rules(prev_rule_count):
    match = None
    matches = []

    for n in range(prev_rule_count + 1):
        match = create_match(rule=getattr(sentinel, 'rule_' + str(n)),
                             previous=match)
        matches.append(match)

    assert isinstance(match.previously_matched_rules, set)
    assert len(match.previously_matched_rules) == prev_rule_count + 1
    assert match.previously_matched_rules == set(m.rule for m in matches)


def test_MatchResult_apply_applies_rule():
    rule = MagicMock(spec=Rule)
    match = create_match(rule=rule)
    match.apply()
    rule.apply.assert_called_once_with(match)


def test_MatchResult_next_match_calls_converter_match():
    converter = MagicMock(spec=Converter)
    converter.match.return_value = sentinel.match_result
    match = create_match(obj=sentinel.obj, converter=converter)

    assert match.next_match() is sentinel.match_result
    converter.match.assert_called_once_with(sentinel.obj, after=match)


@pytest.mark.parametrize('obj_2', [sentinel.obj_2, None])
def test_MatchResult_next_match_with_obj_uses_obj(obj_2):
    converter = MagicMock(spec=Converter)
    match = create_match(obj=sentinel.obj_1, converter=converter)
    match.next_match(obj=obj_2)

    converter.match.assert_called_once_with(obj_2, after=match)


def test_AnyJsonMatcher_matches_JsonValue():
    assert AnyJsonMatcher().match(JsonValue([], 'abc'))[0] is True


@pytest.mark.parametrize('value', ['abc', 1, None, {}, []])
def test_AnyJsonMatcher_doesnt_match_non_JsonValue(value):
    assert AnyJsonMatcher().match(value)[0] is False


@pytest.mark.parametrize('pattern, path, matches', [
    (
        (None, ObjectKey('abc')),
        [ObjectKey('abc')],
        True
    ),
    (
        (ObjectKey('abc'), ObjectKey('foo')),
        [ObjectKey('abc'), ObjectKey('foo')],
        True
    ),
    (
        (ObjectKey('foo'), ArrayIndex(2)),
        [ObjectKey('foo'), ArrayIndex(2)],
        True
    ),
    (
        (ObjectKey('foo'), ArrayIndex(2)),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(2)],
        True
    ),
    (
        (None, ArrayIndex(2)),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(2)],
        False
    ),
    (
        (None, ArrayIndex(2)),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        False
    ),
    (
        (lambda x: True, lambda x: True),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        True
    ),
    (
        (lambda x: False, lambda x: True),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        False
    ),
    (
        (lambda x: True, lambda x: False),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        False
    ),
    (
        (lambda x: False, lambda x: False),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        False
    ),
    (
        (ObjectKey('foo'), lambda x: isinstance(x, ArrayIndex)),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        True
    ),
    (
        (ArrayIndex(1),),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(1)],
        True
    ),
    (
        (ObjectKey('foo'),),
        [ObjectKey('abc'), ObjectKey('foo')],
        True
    ),
    (
        (lambda x: x.value == 'foo',),
        [ObjectKey('abc'), ObjectKey('foo')],
        True
    ),
    (
        (None, None, ArrayIndex(1)),
        [ArrayIndex(1)],
        True
    ),
    (
        (None, None, ObjectKey('foo'),),
        [ObjectKey('foo')],
        True
    ),
    (
        (None, None, lambda x: x.value == 'foo',),
        [ObjectKey('foo')],
        True
    ),
    (
        (ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(2)),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(2)],
        True
    ),
    (
        (ObjectKey('abc'), lambda x: x.value == 'foo', ArrayIndex(2)),
        [ObjectKey('abc'), ObjectKey('foo'), ArrayIndex(2)],
        True
    ),
    (
        (None,),
        [],
        True
    )
])
def test_AncestorsJsonMatcher(pattern, path, matches):
    spm = AncestorsJsonMatcher({pattern: None})

    assert spm.match(JsonValue(path, 'foo'))[0] is matches


def test_AncestorsJsonMatcher_matches_paths_deepest_first():
    '''
    Regression test - paths were being truncated due to mismatching
    length patterns and path-subset-keys passed to zip(). This test would
    previously fail.
    '''
    matcher = AncestorsJsonMatcher({
        (ObjectKey('a'), any_array_element, ObjectKey('b')): sentinel.b,
        (None, None, None, any_array_element): sentinel.c,
    })

    assert not matcher.matches(JsonValue(
        [ObjectKey('a'), ArrayIndex(2), ObjectKey('b'), ObjectKey('c')],
        None))


@pytest.mark.parametrize('index', [0, 1, 4, 10])
def test_any_array_element(index):
    assert any_array_element(ArrayIndex(index)) is True


@pytest.mark.parametrize('val', [1, 2.5, 'abc', True, False, None])
def test_IdentityJsonRule_returns_simple_values_unchanged(val):
    assert (IdentityJsonRule().apply(
                create_match(obj=JsonValue([], val))) is val)


def test_IdentityJsonRule_converts_dict_entries():
    val = {
        'abc': 123,
        'def': [1, 2, 3],
        'ghi': {'foo': 'bar'}
    }
    converter = MagicMock(spec=Converter)
    converter.convert.side_effect = lambda jv: jv.value
    result = IdentityJsonRule().apply(create_match(obj=JsonValue([], val),
                                                   converter=converter))

    assert result == val
    assert result is not val


def test_IdentityJsonRule_converts_list_entries():
    val = ['abc', 1, True]
    converter = MagicMock(spec=Converter)
    converter.convert.side_effect = lambda jv: jv.value
    result = IdentityJsonRule().apply(create_match(obj=JsonValue([], val),
                                                   converter=converter))

    assert result == val
    assert result is not val


@pytest.mark.parametrize('subs, group, path, value, expected', [
    ({'g': {'a': 'b'}}, 'g', [], 'a', 'b'),
    ({'default': {'a': 'b'}}, None, [], 'x', 'x'),
    ({'default': {None: ''}}, None, [ObjectKey('abc')], None, ''),
    ({'default': {'': None}}, None, [ObjectKey('abc')], '', None)
])
def test_ValueSubstitutionJsonRule(pos_matcher, subs, group, path, value,
                                   expected):
    rule = ValueSubstitutionJsonRule(pos_matcher, subs)

    match = MagicMock(spec=MatchedRule)
    match.obj = JsonValue(path, value)
    match.data = group
    # We expect the rule to apply the next rule on the substituted result
    match.next_match().apply.return_value = sentinel.apply_result
    match.reset_mock()

    assert rule.apply(match) == sentinel.apply_result
    match.next_match.assert_called_once_with(JsonValue(path, expected))


@pytest.mark.parametrize('defaults, path, value, expected', [
    ({}, [], {'a': 'b'}, {'a': 'b'}),
    ({'foo': None}, [], {'a': 'b'}, {'a': 'b', 'foo': None}),
    (
        {'foo': None, 'bar': 2, 'a': 'z'},
        [],
        {'a': 'b'},
        {'a': 'b', 'foo': None, 'bar': 2}
    )
])
def test_DefaultPropertiesJsonRule(pos_matcher, defaults, path, value,
                                   expected):
    rule = DefaultPropertiesJsonRule(pos_matcher)

    match = MagicMock(spec=MatchedRule)
    match.obj = JsonValue(path, value)
    match.data = defaults
    # We expect the rule to apply the next rule on the substituted result
    match.next_match().apply.return_value = sentinel.apply_result
    match.reset_mock()

    assert rule.apply(match) == sentinel.apply_result
    match.next_match.assert_called_once_with(JsonValue(path, expected))


def test_json_post_processing_integration():
    input = {
        'foo': 1,
        'bar': {
            'a': 1,
            'b': 2,
            'c': 3,
            'baz': [1, 2, 3, 4]
        }
    }

    converter = Converter([
        ValueSubstitutionJsonRule(
            AncestorsJsonMatcher({
                (None, ObjectKey('foo')): 'group1',
                (ObjectKey('bar'), ObjectKey('a')): None,
                (ObjectKey('bar'), ObjectKey('b')): None,
                (ObjectKey('baz'), ArrayIndex(3)): None,
            }),
            {
                'group1': {
                    1: 'oNe'
                },
                'default': {
                    1: 'One!',
                    2: 'Two?',
                    4: -1
                }
            }),
        DefaultPropertiesJsonRule(
            AncestorsJsonMatcher({
                (None, ObjectKey('bar')): {'e': 4, 'f': 5}
            })),
        IdentityJsonRule()
    ])

    assert converter.convert(JsonValue([], input)) == {
        'foo': 'oNe',
        'bar': {
            'a': 'One!',
            'b': 'Two?',
            'c': 3,
            'baz': [1, 2, 3, -1],
            'e': 4,
            'f': 5
        }
    }
