from unittest.mock import MagicMock, patch, sentinel, call

from lxml import etree

from almauserutils.xmlconversion import Attr
from almauserutils.usersxml2json import (
    usersxml2json, DelegateUserConversionRule, UsersRule)
from .test_xmlconversion import create_match


def test_usersxml2json(users_json, users_xml):
    assert usersxml2json(users_xml) == users_json


def test_DelegateUserConversionRule_calls_usersxml2json(pos_matcher):
    with patch('almauserutils.usersxml2json.userxml2json') as target:
        target.return_value = sentinel.result
        assert DelegateUserConversionRule(pos_matcher).apply(
            create_match(obj='sentinel')) == sentinel.result

        target.assert_called_once_with('sentinel')


def test_UsersRule(pos_matcher):
    el = etree.fromstring('<a x="1" y="2"><b/><c/></a>')

    def convert(x):
        if isinstance(x, Attr):
            return (x.name, x)
        return x
    converter = MagicMock()
    converter.convert.side_effect = convert

    result = UsersRule(pos_matcher).apply(
        create_match(obj=el, converter=converter))

    converter.convert.assert_has_calls([
            call(el.find('b')),
            call(el.find('c')),
            call(Attr(el, 'x')),
            call(Attr(el, 'y'))
        ], any_order=True)

    assert result == {
        'user': [el.find('b'), el.find('c')],
        'x': Attr(el, 'x'),
        'y': Attr(el, 'y')
    }
