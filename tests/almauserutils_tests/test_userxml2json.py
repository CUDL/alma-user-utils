from almauserutils.userxml2json import userxml2json


def test_userxml2json(user_json, user_xml):
    assert userxml2json(user_xml) == user_json
