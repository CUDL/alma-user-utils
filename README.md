# Alma User Utils

Command line programs/Python libraries for working with ExLibris Alma user exports.

## Install

Assuming you have SSH authentication set up on BitBucket:

```
$ pip install git+ssh://git@bitbucket.org/CUDL/alma-user-utils.git
```

## Commands

The following commands are available:

### `filterusers`

Extract a set of users (identified by one of any identifier type) from one or
more Alma user export XML files. The extracted users are in the form of an Alma
users XML file, suitable for importing into Alma.

See `$ filterusers --help` for details.

#### Example: Extracting one user

```
$ time filterusers -vv -i primary:hwtb2@cam.ac.uk /var/sftp/alma/user-export-cam/EXPORT_USERS-* > users-hwtb2.xml
Found 1 IDs:
    primary:hwtb2@cam.ac.uk

Matched 1 of 145232 users

$ head users-hwtb2.xml
<users>
  <user>
    <record_type desc="Staff">STAFF</record_type>
    <primary_id>hwtb2@cam.ac.uk</primary_id>
    <first_name>Hal</first_name>
    <middle_name/>
    <last_name>Blackburn</last_name>
    <full_name>Hal Blackburn</full_name>
    <pin_number/>
    <user_title desc="The Rev Father">THE REV FATHER</user_title>
$
```

#### Example: Extracting multiple users from a file listing IDs

```
$ time filterusers -v -o ./training-users.xml --ids-from ./users.txt /var/sftp/alma/user-export-cam/EXPORT_USERS-*
Found 149 IDs
Matched 149 of 145232 users

real    0m15.242s
user    0m14.948s
sys     0m0.292s

$ head users.txt
primary:se01
primary:se02
primary:se03
primary:se04
primary:se05
primary:se06
primary:se07
primary:se08
primary:se09
primary:se10

$ head training-users.xml
<users>
  <user>
    <record_type desc="Public">PUBLIC</record_type>
    <primary_id>aa3</primary_id>
    <first_name>Annie</first_name>
    <middle_name/>
    <last_name>Acumen</last_name>
    <full_name>Annie Acumen</full_name>
    <pin_number/>
    <user_title desc=""/>
```
