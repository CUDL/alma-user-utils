from setuptools import setup, find_packages


def get_version(filename):
    """
    Parse the value of the __version__ var from a Python source file
    without running/importing the file.
    """
    import re
    version_pattern = r'^ *__version__ *= *[\'"](\d+\.\d+\.\d+)[\'"] *$'
    match = re.search(version_pattern, open(filename).read(), re.MULTILINE)

    assert match, ('No version found in file: {!r} matching pattern: {!r}'
                   .format(filename, version_pattern))

    return match.group(1)


setup(
    name='alma-user-utils',
    version='1.2.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    author='Hal Blackburn',
    author_email='hwtb2@cam.ac.uk',
    install_requires=['lxml', 'docopt', 'simplejson'],
    entry_points={
        'console_scripts': ['filterusers=almauserutils.filterusers:main',
                            'userxml2json=almauserutils.userxml2json_cmd:main',
                            'patron_info=almauserutils.sip2_patron_info:main']
    },
    package_data={
        'almauserutils': ['data/*.xsd'],
    }
)
