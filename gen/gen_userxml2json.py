'''
This module is responsible for generating the Alma user XML to JSON conversion
in src/userxml2json.py.

The approach taken is to derive the conversion from the XSD schema Ex-Libris
provide for the user XML (src/almauserutils/data/rest_user.xsd). The reasoning
being:

  - Generating all the conversions should reduce the risk of human error, which
    would be likely if manually coding the entire conversion of hundreds of
    fields
  - The conversion can be updated automatically if the schema changes

However it turns out there seem to be a details in the conversion Ex-Libris
implement which are not deducible from the schema. These have been hard coded
and applied in a second post-processing step after the initial XML->JSON
conversion.

Knowing what I do now, I'd probably just implement a set of classes to
represent Alma users with operations to map to/from JSON and XML. I didn't do
that because it seemed like this approach would be quick and solve our
immediate problem. Meh.
'''

from collections import namedtuple
import itertools
from os import path

from lxml import etree

from almauserutils.xmlconversion import (
    ObjectKey, ArrayIndex, any_array_element)

namespaces = {'xs': 'http://www.w3.org/2001/XMLSchema'}

xs_element = etree.QName(namespaces['xs'], 'element')
xs_schema = etree.QName(namespaces['xs'], 'schema')
xs_simple_type = etree.QName(namespaces['xs'], 'simpleType')


class Element(namedtuple('Element',
                         'parent name category data_type singular_name')):
    def to_json(self):
        result = {
            'parent': self.parent,
            'name': self.name,
            'category': self.category,
            'data_type': self.data_type
        }

        if self.singular_name is not None:
            result['singularName'] = self.singular_name

        return result


class Attribute(namedtuple('Attribute', 'parent host name data_type')):
    def to_json(self):
        return {
            'parent': self.parent,
            'host': self.host,
            'name': self.name,
            'data_type': self.data_type
        }


def xp(el, expr):
    return el.xpath(expr, namespaces=namespaces)


def validate_elements_unique_with_parent(xsd):
    elements = xp(xsd, '//xs:element')

    element_identifiers = set()
    for el in elements:
        name = el.attrib['name']
        parent = parent_name(el)

        ident = (parent, name)
        assert ident not in element_identifiers, (
            'Element {} not unique at line: {}'.format(name, el.sourceline))
        element_identifiers.add(ident)


def parent_el(xs_el):
    context = xp(xs_el, 'ancestor::xs:complexType|parent::xs:schema')
    assert len(context), ('no context found for xs:element: {} on line {}'
                          .format(xs_el.attrib['name'], xs_el.sourceline))
    enclosing_type = context[-1]

    if enclosing_type.tag == xs_schema:
        return None

    if 'name' in enclosing_type.attrib:
        parents = [e for e in xp(xs_el.getroottree(), '//xs:element[@type]')
                   if e.attrib['type'] == enclosing_type.attrib['name']]

        if len(parents) > 1:
            raise ValueError(
                'element referenced from multiple contexts: {} at line {}'
                .format(xs_el.attrib['name'], xs_el.sourceline))

        return parents[0]
    else:
        parent = enclosing_type.getparent()
        assert parent.tag == xs_element
        return parent


def parent_name(xs_el):
    el = parent_el(xs_el)

    return None if el is None else el.attrib['name']


def get_type(xs_el):
    if 'type' in xs_el.attrib:
        type_name = xs_el.attrib['type']
        if type_name.startswith('xs:'):
            return type_name

        type_el = next(e for e in xp(xs_el.getroottree(),
                                     '/xs:schema/xs:complexType[@name]|'
                                     '/xs:schema/xs:simpleType[@name]')
                       if e.attrib['name'] == type_name)
    else:
        type_el = next(iter(xp(xs_el, 'xs:complexType|xs:simpleType')))

    if type_el.tag == xs_simple_type:
        return xp(type_el, 'xs:restriction/@base')[0]
    return type_el


def value_type(xs_el):
    match = xp(xs_el,
               '@type|xs:complexType/xs:simpleContent/xs:extension/@base')
    if len(match) != 1:
        raise ValueError('Unable to determine type of element: {} at line {}'
                         .format(xs_el.attrib['name'], xs_el.sourceline))
    type_name = match[0]

    if type_name == 'xs:boolean':
        return 'bool'
    else:
        return 'str'


def resolve_base_type(xsd, type_name):
    type_el = next(e for e in xp(xsd, '/xs:schema/xs:simpleType[@name]')
                   if e.attrib['name'] == type_name)

    return xp(type_el, 'xs:restriction/@base')[0]


def get_data_type(xsd, type_name):
    if not type_name.startswith('xs:'):
        type_name = resolve_base_type(xsd, type_name)

    return {
        'xs:string': 'str',
        'xs:int': 'int',
        'xs:boolean': 'bool',
        'xs:float': 'float',
        'xs:date': 'str',
        'xs:dateTime': 'str'
    }[type_name]


def get_element_info(xs_el):
    # 4 categories of element:
    # internal, list, leaf and leaf w/ attributes
    el_type = get_type(xs_el)

    singular_name = None
    if isinstance(el_type, str):
        category = 'leaf'
        data_type = get_data_type(xs_el.getroottree(), el_type)
    elif xp(el_type, 'xs:simpleContent/xs:extension[@base]'):
        category = 'leaf-with-attributes'
        data_type = get_data_type(
            xs_el.getroottree(),
            xp(el_type, 'xs:simpleContent/xs:extension/@base')[0])
    elif xp(el_type, 'count(xs:sequence/xs:element) = 1 and '
                     'xs:sequence/xs:element[@maxOccurs="unbounded"]'):
        category = 'list'
        singular_name = xp(el_type, 'xs:sequence/xs:element/@name')[0]
        data_type = None
    elif xp(el_type, 'xs:all/xs:element or count(xs:sequence/xs:element) > 1'):
        category = 'internal'
        data_type = None
    else:
        raise ValueError('Unable to categorise element: {} on line {}'
                         .format(xs_el.attrib['name'], xs_el.sourceline))

    return Element(parent_name(xs_el), xs_el.attrib['name'], category,
                   data_type, singular_name)


def get_attribute_info(xs_attr):
    host_el = parent_el(xs_attr)
    parent = parent_name(host_el)

    return Attribute(parent, host_el.attrib['name'],
                     xs_attr.attrib['name'],
                     get_data_type(xs_attr.getroottree(),
                                   xs_attr.attrib['type']))


def ident_repr(ident):
    parent, el, type = ident

    if parent is None:
        path = '/{}'.format(el)
    else:
        path = '{}/{}'.format(parent, el)

    if type is not None:
        return '{} ({})'.format(path, type)
    return path


def get_json_conversion_info(xsd):
    return {
        'elements': [
            get_element_info(e).to_json()
            for e in xp(xsd, '//xs:element')
        ],
        'attributes': [
            get_attribute_info(a).to_json()
            for a in xp(xsd, '//xs:attribute')
        ]
    }


def render_el_patterns(el_infos, category, data=lambda e: None, indent=4):
    infos = (e for e in el_infos if e.category == category)
    patterns = (((e.parent, e.name), data(e)) for e in infos)

    return '\n'.join('{}{!r},'.format(' ' * indent, p) for p in patterns)


def render_attr_patterns(attr_infos, indent=4):
    patterns = (((a.parent, a.host, a.name), a.data_type) for a in attr_infos)

    return '\n'.join('{}{!r},'.format(' ' * indent, p) for p in patterns)


def render_substitution_patterns(patterns, group=None, indent=4):
    return '\n'.join('{}{!r}: {!r},'.format(' ' * indent,
                                            tuple(render_pattern_value(x)
                                                  for x in p),
                                            group)
                     for p in patterns)


def render_pattern_value(val):
    if isinstance(val, (ObjectKey, ArrayIndex)):
        return val
    elif val is None:
        return None
    elif val is any_array_element:
        return Literal('any_array_element')
    raise ValueError('Unsupported value: {!r}'.format(val))


class Literal:
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return str(self.value)


def render_defaults_patterns(patterns, indent=4):
    return '\n'.join('{}{!r}: {!r},'.format(
                         ' ' * indent,
                         tuple(render_pattern_value(x) for x in k),
                         v)
                     for k, v in patterns)


_template_header = '''\
###########################################################################
# THIS FILE WAS AUTO-GENERATED BY gen_userxml2json.py DO NOT MODIFY BY HAND
###########################################################################

'''


def render_template(el_info, attr_info):
    with open(path.join(path.dirname(__file__),
                        './data/userxml2json.py.template')) as f:
        template = _template_header + ''.join(
            itertools.dropwhile(lambda l: l.startswith('#'), f))

    values = {
        'leaf_patterns':
            render_el_patterns(el_info, 'leaf', data=lambda e: e.data_type),
        'leaf_attr_patterns':
            render_el_patterns(el_info, 'leaf-with-attributes',
                               data=lambda e: e.data_type),
        'internal_patterns': render_el_patterns(el_info, 'internal'),
        'list_patterns':
            render_el_patterns(el_info, 'list',
                               data=lambda e: e.singular_name),
        'attr_patterns':
            render_attr_patterns(attr_info),
        'empty_str_to_none_patterns':
            render_substitution_patterns(empty_str_to_none_patterns,
                                         group='empty-str', indent=12),
        'default_json_properties_patterns':
            render_defaults_patterns(json_defaults, indent=8)

    }

    return template.format(**values)


# These two sets of patterns are hard coded, as they don't seem to be
# represented in the schema.
empty_str_to_none_patterns = [
    (ObjectKey('campus_code'), ObjectKey('value')),
    (
        ObjectKey('user_role'), any_array_element, ObjectKey('scope'),
        ObjectKey('value')
    ),
]

json_defaults = [
    ((ObjectKey('email'), any_array_element), {'description': None}),
    (
        (ObjectKey('user_role'), any_array_element, ObjectKey('scope')),
        {'desc': None}
    ),
    # Root
    ((None,), {'fees': None, 'link': None, 'loans': None, 'requests': None})
]


def main():
    '''
    Generate userxml2json (on stdout) by rendering the template with conversion
    rules extracted automatically from the XSD schema.
    '''
    xsd = etree.parse(path.join(path.dirname(__file__),
                      '../src/almauserutils/data/rest_user.xsd'))
    validate_elements_unique_with_parent(xsd)

    el_info = [get_element_info(e) for e in xp(xsd, '//xs:element')]
    attr_info = [get_attribute_info(a) for a in xp(xsd, '//xs:attribute')]

    print(render_template(el_info, attr_info), end='')


if __name__ == '__main__':
    main()
